defmodule Torx.CLI do
  @moduledoc false

  @config_path "#{File.cwd!()}/lib/config.json"

  require Logger
  alias TableRex.Table
  alias Torx.Torrent

  def main(argv) do
    {options, _, _} =
      OptionParser.parse(argv,
        switches: [
          provider: :string,
          search: :string,
          page: :integer,
          per: :integer,
          username: :string,
          password: :string
        ],
        aliases: [
          p: :provider,
          s: :search,
          n: :page,
          c: :per,
          u: :username,
          w: :password
        ]
      )

    search(options)
  end

  defp search(options) do
    provider =
      Enum.find(Map.get(Jason.decode!(File.read!(@config_path)), "providers"), fn x ->
        x["name"] == (options[:provider] || "1337x")
      end)

    if is_nil(provider) do
      reason = "Provider #{options[:provider]} not found!"
      exit(reason)
    end

    fetch_url =
      provider["url"]
      |> String.replace(~r/\{query\}/, options[:search])
      |> String.replace(~r/\{page\}/, Integer.to_string(options[:page] || 1))
      |> String.replace(~r/\{per\}/, Integer.to_string(options[:per] || 25))

    case HTTPoison.get(fetch_url) do
      {:ok, response} ->
        case response.status_code do
          200 ->
            torrents =
              response.body
              |> Floki.find(provider["wrapper"])
              |> Floki.find(provider["item"])
              |> Enum.map(&extract_torrent_info(&1, provider))
              |> Enum.map(fn torrent ->
                [
                  Map.get(torrent, :title),
                  Map.get(torrent, :seeds),
                  Map.get(torrent, :leeches),
                  Map.get(torrent, :size),
                  Map.get(torrent, :uploader),
                  Map.get(torrent, :time)
                ]
              end)

            Table.new(
              torrents,
              ~w(title seeds leeches size uploader time),
              "Torrents found in " <> provider["name"] <> " for " <> options[:search]
            )
            |> Table.render!()
            |> IO.puts()

          _ ->
            :error
        end

      _ ->
        :error
    end
  end

  defp extract_torrent_info({_tag, _attrs, children}, provider) do
    raw_html = Floki.raw_html(children)

    title =
      raw_html
      |> Floki.find(provider["data"]["title"])
      |> hd()
      |> Floki.text()

    {torrent_file, magnet_link} = get_torrents(raw_html, provider)

    seeds =
      try do
        raw_html
        |> Floki.find(provider["data"]["seeds"])
        |> hd()
        |> Floki.text()
      rescue
        _ -> nil
      end

    leeches =
      try do
        raw_html
        |> Floki.find(provider["data"]["leeches"])
        |> hd()
        |> Floki.text()
      rescue
        _ -> nil
      end

    size =
      try do
        raw_html
        |> Floki.find(provider["data"]["size"])
        |> hd()
        |> Floki.text()
      rescue
        _ -> ""
      end

    uploader =
      try do
        raw_html
        |> Floki.find(provider["data"]["uploader"])
        |> hd()
        |> Floki.text()
      rescue
        _ -> ""
      end

    time =
      try do
        raw_html
        |> Floki.find(provider["data"]["time"])
        |> hd()
        |> Floki.text()
        |> String.replace(~r/(th|nd|rd|st)/, "")
        |> maybe_parse_time(provider["options"]["time_parser"])
      rescue
        _ -> nil
      end

    torrent = %Torrent{
      title: String.trim(title),
      seeds: seeds,
      leeches: leeches,
      size: size,
      uploader: String.trim(uploader),
      torrent_file: torrent_file,
      magnet_link: magnet_link,
      time: time
    }

    torrent
  end

  defp maybe_parse_time(strtime, parser) when is_nil(parser), do: strtime

  defp maybe_parse_time(strtime, parser) do
    case Timex.parse(strtime, parser, :strftime) do
      {:ok, parsed_time} ->
        {:ok, formatted_time} = Timex.format(parsed_time, "%e %b %Y", :strftime)
        formatted_time

      _ ->
        strtime
    end
  end

  defp get_torrents(_, %{"options" => %{"download" => %{"required_detail_page" => bool}}})
       when bool == false,
       do: {nil, nil}

  defp get_torrents(raw_html, provider) do
    detail_url =
      raw_html
      |> Floki.find(provider["options"]["download"]["detail_page_url"])
      |> hd()
      |> Floki.attribute("href")
      |> hd()
      |> ensure_has_base_url(provider["url"])

    case HTTPoison.get(detail_url) do
      {:ok, response} ->
        case response.status_code do
          200 ->
            body = response.body

            torrent =
              try do
                body
                |> Floki.find(provider["options"]["download"]["urls"]["torrent"])
                |> hd()
                |> Floki.attribute("href")
                |> hd()
              rescue
                _ -> nil
              end

            magnet =
              try do
                body
                |> Floki.find(provider["options"]["download"]["urls"]["magnet"])
                |> hd()
                |> Floki.attribute("href")
                |> hd()
              rescue
                _ -> nil
              end

            {torrent, magnet}

          _ ->
            Logger.error("hata")
            {nil, nil}
        end

      {:error, reason} ->
        IO.inspect(reason)
        Logger.error("hata")
        {nil, nil}
    end
  end

  defp ensure_has_base_url(page_url, url) do
    parsed_url = URI.parse(url)

    case Regex.scan(~r/^https?:/, page_url) do
      [] ->
        URI.merge(URI.parse(parsed_url.scheme <> "://" <> parsed_url.host), page_url)
        |> to_string()

      _ ->
        page_url
    end
  end
end
