defmodule Torx do
  @moduledoc """
  Documentation for Torx.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Torx.hello()
      :world

  """
  def hello do
    :world
  end
end
