defmodule Torx.MixProject do
  use Mix.Project

  def project do
    [
      app: :torx,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      escript: [main_module: Torx.CLI],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :timex, :httpoison],
      mod: {Torx.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:httpoison, "~> 1.5.0"},
      {:floki, "~> 0.21"},
      {:tzdata, "~> 0.1.8", override: true},
      {:timex, "~> 3.1"},
      {:table_rex, "~> 2.0.0"}
    ]
  end
end
